# README #

## How to run ##

```sh
$ java -jar VendingMachine.jar "./resources/command.txt"
```
Passing a path contains commands set as parameter

## Config file ##

./resources/stock_products.txt - for keep products data.

format: &lt;id&gt;,&lt;name&gt;,&lt;price_in_cent&gt;,&lt;stocking_quantity&gt;

```
1,Cola,100,9
2,Chips,50,2
3,Candy,65,2
```

./resources/stock_coins.txt - for keep coins data.

format: &lt;name&gt;,&lt;stocking_quantity&gt;

```
Quarter,4
Nickel,4
Dime,2
```

## Command ##

```
showProducts
```

```
selectProduct <productId>
```

- productId reference to stock_products.txt

```
acceptCoins <diameter> <thickness> <mass>
```

- diameter to recognize a coin

- thickness to recognize a coin

- mass to recognize a coin

[Coins of the United States dollar](https://en.wikipedia.org/wiki/Coins_of_the_United_States_dollar)

```
returnCoins
```
