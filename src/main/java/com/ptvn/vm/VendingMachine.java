package com.ptvn.vm;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ptvn.vm.entity.Coin;
import com.ptvn.vm.entity.Dime;
import com.ptvn.vm.entity.Nickel;
import com.ptvn.vm.entity.Penny;
import com.ptvn.vm.entity.Product;
import com.ptvn.vm.entity.Quarter;

public class VendingMachine {

	public static void main(String[] args) {
		try {
			VendingMachine vendor = new VendingMachine();

		    List<String> lines = Files.readAllLines(Paths.get(args[0]));
		    for (String command : lines) {
		    	String[] params = command.split(" ");
		    	if (params.length>0) {
			    	if (params[0].equalsIgnoreCase("showProducts")) {
			    		
						vendor.showProducts();
						
			    	} else if (params[0].equalsIgnoreCase("selectProduct")) {
			    		
			    		if (params.length<2) {
			    			throw new Exception("productId is required");
			    		}
						vendor.selectProduct(Integer.parseInt(params[1]));
						
			    	} else if (params[0].equalsIgnoreCase("acceptCoins")) {
			    		
			    		if (params.length<4) {
			    			throw new Exception("diameter, thickness and mass is required");
			    		}
						vendor.acceptCoins(Double.parseDouble(params[1]), Double.parseDouble(params[2]), Double.parseDouble(params[3]));
						
			    	} else if (params[0].equalsIgnoreCase("returnCoins")) {
			    		
						vendor.returnCoins();
			    	}
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private final String DELIMITER_STOCK = ",";
	private final String PATH_STOCK_COINS = "./resources/stock_coins.txt";
	private final String PATH_STOCK_PRODUCTS = "./resources/stock_products.txt";
	private final String PATH_TEMP = "./resources/temp.txt";
	
	List<Product> stockProducts;
	Map<String, String> stockCoins;
	Map<String, String> temp = new HashMap<String, String>();
	
	public boolean acceptCoins(double diameter, double thickness, double mass) throws IOException {
		boolean isValid = true;
		Coin coinInput = null;
		if (diameter==Penny.DIAMETER && thickness==Penny.THICKNESS && mass==Penny.MASS) {
			isValid = false;
		} else {
			if (diameter==Nickel.DIAMETER && thickness==Nickel.THICKNESS && mass==Nickel.MASS) {
				coinInput = new Nickel();
			} else if (diameter==Dime.DIAMETER && thickness==Dime.THICKNESS && mass==Dime.MASS) {
				coinInput = new Dime();
			} else if (diameter==Quarter.DIAMETER && thickness==Quarter.THICKNESS && mass==Quarter.MASS) {
				coinInput = new Quarter();
			}
		}
		
		checkAmount(coinInput);
		
		return isValid;
	}
	
	public void selectProduct(int productId) throws IOException {
		Product item;
		getStockProducts();
		readTemp();
		
		if (temp.containsKey("productId")) {
			reStockProducts(Integer.parseInt(temp.get("productId")));
		}
		
		item = findStockProducts(productId);

		if (item!=null) {
			if (item.getQuantity()>0) {
				int quantity = item.getQuantity() - 1;
				updateStockProducts(productId, quantity);
				
				//update temp
			    temp.put("productId", Integer.toString(productId));
			    temp.put("price", Integer.toString(item.getPrice()));
			    writeTemp();
			} else {
				System.out.println("SOLD OUT");
			}
		}
		
		checkAmount(null);
	}
	
	public void returnCoins() throws IOException {
		readTemp();
		getStockCoins();
		int productId = temp.containsKey("productId") ? Integer.parseInt(temp.get("productId")) : 0;
		
		String[] names = {"Nickel", "Dime", "Quarter"};
		for (String key : names) {
			if (stockCoins.containsKey(key) && temp.containsKey(key)) {
				int quantity = Integer.parseInt(stockCoins.get(key))-Integer.parseInt(temp.get(key));
				stockCoins.put(key, Integer.toString(quantity));
			}
		}
		
		//clear temp
		temp = new HashMap<String, String>();
		writeTemp();
		
		if (productId>0) {
			getStockProducts();
			reStockProducts(productId);
		}
		
		updateStockCoins();
		
		System.out.println("INSERT COIN");
	}

	public void showProducts() throws IOException {
		getStockProducts();
		stockProducts.forEach(item -> {
			System.out.println(String.format("%s : $%s", item.getName(), toDollar(item.getPrice())));
		});
	}

	private void checkAmount(Coin coin) throws IOException {
		String msg = "INSERT COIN";
		readTemp();
		getStockCoins();
		
		if (getStockCoinsQuantity("Nickel")==0 && getStockCoinsQuantity("Dime")==0 && getStockCoinsQuantity("Quarter")==0) {
			msg = "EXACT CHANGE ONLY";
		}
		
		int amount = temp.containsKey("amount") ? Integer.parseInt(temp.get("amount")) : 0;
		int price = temp.containsKey("price") ? Integer.parseInt(temp.get("price")) : 0;
		if (coin!=null) {
			amount += coin.getCent();
			String key = coin.getClass().getSimpleName();
			
			stockCoins.put(key, stockCoins.containsKey(key) ? (Integer.parseInt(stockCoins.get(key))+1)+"" : "1");
			
			int quantity = temp.containsKey(key) ? Integer.parseInt(temp.get(key))+1 : 1;
			if (amount<price || !temp.containsKey("productId")) {
				temp.put(key, Integer.toString(quantity));
				temp.put("amount", Integer.toString(amount));
				writeTemp();
			} else {
				int change = amount - price;
				Coin[] arr = {new Quarter(), new Dime(), new Nickel()};
				for (Coin c : arr) {
					change = calculateCoinChange(c, change);
				}
				
				if (change==0) {
					//clear temp
					temp = new HashMap<String, String>();
					writeTemp();
					
					msg = String.format("CHANGE : $%s\nTHANK YOU", Integer.toString(amount - price));
				} else {
					msg = "COINS NOT ENOUGH FOR CHANGE";
				}
			}
			
			updateStockCoins();
		}
		System.out.println(String.format("PRICE : $%s\nAMOUNT : $%s\n%s", toDollar(price), toDollar(amount), msg));
	}
	
	private int calculateCoinChange(Coin coin, int change) throws IOException {
		int stock = getStockCoinsQuantity(coin.getClass().getSimpleName());
		if (change>=coin.getCent() && stock>0) {
			int qty = change/coin.getCent();
			coin.setQuantity(qty<0?0:qty);
			stockCoins.put(coin.getClass().getSimpleName(), Integer.toString(stock-coin.getQuantity()));
			change -= coin.getQuantity()*coin.getCent();
		}
		return change;
	}

	private int getStockCoinsQuantity(String sName) {
		return stockCoins.containsKey(sName) ? Integer.parseInt(stockCoins.get(sName)) : 0;
	}

	private String toDollar(int cent) {
		return new DecimalFormat("0.00").format(Float.valueOf(cent)/100);
	}
	
	private void readTemp() throws IOException {
		Path pathTemp = Paths.get(PATH_TEMP);
		if (Files.exists(pathTemp)) {
		    List<String> lines = Files.readAllLines(pathTemp);
		    lines.forEach(s -> {
		    	String[] arr = s.split("=");
		    	String key = arr[0];
		    	String val = arr.length>1 ? arr[1] : "";
		    	temp.put(key, val);
		    });
		}
	}

	private void writeTemp() throws IOException {
		List<String> lines = new ArrayList<String>();
		for(Map.Entry<String, String> entry : temp.entrySet()) {
			lines.add(entry.toString());
		}
	    Files.write(Paths.get(PATH_TEMP), String.join(System.lineSeparator(), lines).getBytes());
	}

	private void getStockCoins() throws IOException {
		stockCoins = new HashMap<String, String>();
	    List<String> lines = Files.readAllLines(Paths.get(PATH_STOCK_COINS));
	    lines.forEach(str -> {
    		String[] arr = str.split(DELIMITER_STOCK);
    		stockCoins.put(arr[0], (arr.length>1 ? arr[1] : ""));
    	});
	}

	private void updateStockCoins() throws IOException {
		List<String> lines = new ArrayList<String>();
		for(Map.Entry<String, String> entry : stockCoins.entrySet()) {
			lines.add(entry.toString());
		}
	    Files.write(Paths.get(PATH_STOCK_COINS), String.join(System.lineSeparator(), lines).replaceAll("=", ",").getBytes());
	}

	private void getStockProducts() throws IOException {
		stockProducts = new ArrayList<Product>();
	    List<String> lines = Files.readAllLines(Paths.get(PATH_STOCK_PRODUCTS));
	    lines.forEach(str -> {
    		String[] values = str.split(DELIMITER_STOCK);
    		Product item = new Product(Integer.parseInt(values[0]), values[1], Integer.parseInt(values[2]), Integer.parseInt(values[3]));
    		stockProducts.add(item);
    	});
	}

	private void updateStockProducts(int productId, int quantity) throws IOException {
		List<String> lines = new ArrayList<String>();
		stockProducts.forEach(p -> {
	    	if (productId==p.getId()) {
	    		p.setQuantity(quantity);
	    	}
	    	lines.add(MessageFormat.format("{1}{0}{2}{0}{3}{0}{4}", DELIMITER_STOCK, p.getId(), p.getName(), p.getPrice(), p.getQuantity()));
	    });
	    Files.write(Paths.get(PATH_STOCK_PRODUCTS), String.join(System.lineSeparator(), lines).getBytes());
	}

	private void reStockProducts(int productId) throws IOException {
		Product item = findStockProducts(productId);
		if (item!=null) {
			updateStockProducts(productId, item.getQuantity() + 1);
		}
	}

	private Product findStockProducts(int productId) throws IOException {
		return stockProducts.stream()
				  .filter(p -> productId==p.getId())
				  .findAny().orElse(null);
	}
}
