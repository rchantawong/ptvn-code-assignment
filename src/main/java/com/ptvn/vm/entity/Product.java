package com.ptvn.vm.entity;

public class Product {

	private int id;
	private String name;
	private int price;
	private int quantity;
	
	public Product() {
		
	}
	
	public Product(int id, String name, int price, int quantity) {
		setId(id);
		setName(name);
		setPrice(price);
		setQuantity(quantity);
	}
	
	public int getId() {
		return id;
	}
	private void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	private void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	private void setPrice(int price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
