package com.ptvn.vm.entity;

public class Penny extends Coin {
	public static final double DIAMETER;
	public static final double THICKNESS;
	public static final double MASS;
	public static final int CENT;
	public static final boolean IS_VALID;
	
	private static final int quantity;

	static {
		DIAMETER = 19.05;
		THICKNESS = 1.52;
		MASS = 2.5;
		CENT = 1;
		IS_VALID = false;
		
		quantity = 0;
	}
	
	public Penny() {
		super(DIAMETER, THICKNESS, MASS, CENT, IS_VALID, quantity);
	}
}
