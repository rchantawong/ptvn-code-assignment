package com.ptvn.vm.entity;

public class Coin {

	private static double diameter;
	private static double thickness;
	private static double mass;
	private static int cent;
	private static boolean isValid;
	private static int quantity;
	
	public Coin() {
		
	}
	
	Coin(double diameter, double thickness, double mass, int cent, boolean isValid, int quantity) {
		setDiameter(diameter);
		setThickness(thickness);
		setMass(mass);
		setCent(cent);
		setValid(isValid);
		setQuantity(quantity);
	}
	
	public double getDiameter() {
		return diameter;
	}
	public void setDiameter(double diameter) {
		Coin.diameter = diameter;
	}
	public double getThickness() {
		return thickness;
	}
	public void setThickness(double thickness) {
		Coin.thickness = thickness;
	}
	public double getMass() {
		return mass;
	}
	public void setMass(double mass) {
		Coin.mass = mass;
	}
	public int getCent() {
		return cent;
	}
	public void setCent(int cent) {
		Coin.cent = cent;
	}
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		Coin.isValid = isValid;
	}
	public static int getQuantity() {
		return quantity;
	}
	public static void setQuantity(int quantity) {
		Coin.quantity = quantity;
	}
}
