package com.ptvn.vm.entity;

public class Nickel extends Coin {
	public static final double DIAMETER;
	public static final double THICKNESS;
	public static final double MASS;
	public static final int CENT;
	public static final boolean IS_VALID;
	
	private static int quantity;

	static {
		DIAMETER = 21.21;
		THICKNESS = 1.95;
		MASS = 5.0;
		CENT = 5;
		IS_VALID = true;
		
		quantity = 0;
	}
	
	public Nickel() {
		super(DIAMETER, THICKNESS, MASS, CENT, IS_VALID, quantity);
	}
	
	public static int getQuantity() {
		return quantity;
	}
	
	public static void setQuantity(int quantity) {
		Nickel.quantity = quantity;
	}

	public double getDiameter() {
		return DIAMETER;
	}

	public double getThickness() {
		return THICKNESS;
	}

	public double getMass() {
		return MASS;
	}

	public int getCent() {
		return CENT;
	}

	public boolean isValid() {
		return IS_VALID;
	}
}
