package com.ptvn.vm.entity;

public class Dime extends Coin {
	public static final double DIAMETER;
	public static final double THICKNESS;
	public static final double MASS;
	public static final int CENT;
	public static final boolean IS_VALID;
	
	private static int quantity;

	static {
		DIAMETER = 17.91;
		THICKNESS = 1.35;
		MASS = 2.268;
		CENT = 10;
		IS_VALID = true;
		
		quantity = 0;
	}
	
	public Dime() {
		super(DIAMETER, THICKNESS, MASS, CENT, IS_VALID, quantity);
	}
	
	public static int getQuantity() {
		return quantity;
	}
	
	public static void setQuantity(int quantity) {
		Dime.quantity = quantity;
	}

	public double getDiameter() {
		return DIAMETER;
	}

	public double getThickness() {
		return THICKNESS;
	}

	public double getMass() {
		return MASS;
	}

	public int getCent() {
		return CENT;
	}

	public boolean isValid() {
		return IS_VALID;
	}
}
